#include <stdio.h>
#include <unistd.h> //sleep

#include "timer.h"

using namespace std;

/** the TestClass is a class designed to both test and demonstrate
 * how to use the Timer class. You can inherit or instantiate  the
 * Timer class but you must provide a callback function (s) and a 
 * reference to the class in which the callback is in.
 *  
 * This class instantiates an object of the Timer class and then sets
 * up a number of different ways to demonstrate the usage of the timer
 * class  */
class TestClass// : public TimerCallback
{
public:
  TestClass() {
    /**create an instance of the timer class. The callback for the timer will be in
     * this class, therefore we, create the templated timer with TestClass and
     * provide the 'this' reference to ourself. Next parameter is what function
     * we want our callback to be within the class specified, followed by the 
     * default timer time (1 second), and set autoreset to false.  The default timer 
     * time and the autoreset parameters are optional.   */
    timer = new Timer<TestClass>(this, &TestClass::TimerTimeout1, false, 1000); 
    }
  ~TestClass() {}

/** gets called by the timer on timeout 
 * to demonstrate the simple timer and
 * AutoRest timer functionality */
  void TimerTimeout1()
  {
    /* Do something here */
    printf("Timer thread is in the TimerTimeout1 callback\n");
  }

/** gets called by the timer on timeout 
 * to demonstrate the timers ability
 * to set it's own new timeout times.
 * Once such case would be a timer that
 * needs to change it timeout time for
 * blinking text */
  void TimerTimeout2()
  {
    /* Do something here */
    if (timer->getTime() == 2000) //get the current timeout time
      {
        timer->setTime(500);  //set new timeout time to 500ms
        printf("Timer thread is in the TimerTimeout2 callback. Long timer time expired. Self reset with Short time\n");
      }
      else
      {
        timer->setTime(2000);  //set new timeout time to 2 seconds
        printf("Timer thread is in the TimerTimeout2 callback. Short timer time expired. Self reset with long time\n");
      }
  }

  void start()
  {

    /** This first example demonstrates a simple,
     * singly run timer. Create a timer and it
     * will run one time and stop.  */
    printf("\nDemonstrate simple timer.\n");
    timer->setTime(2000); //set timer for 2 seconds
    timer->Start(); //start timer
    sleep(5); //pause main thread long enough to ensure timer has timed out



    /** This second example demonstrates a timer that autoresets
     * itself. This is accomplished by setting the autoreset
     * variable to true. The timer will run, timeout, call
     * the callback, and then run again automatically until
     * Stop() is called. If autorest is set to false, it 
     * will simply run until it times out, but then never
     * start back up again.   */
    printf("\nDemonstrate timer with auto reset.\n");
    timer->AutoReset(true);  //set the timer to automatically restart
    timer->setTime(1000);   //set the timer timeout time to 1 second
    timer->Start();   //start the timer



    sleep(6); //sleep main thread for 6 seconds allowing the timer to timeout 5 times

    timer->Stop(); //after using stop, must still wait the remaining amount of time for timer timeout before calling start again. Otherwise timer will resume
    timer->AutoReset(false);  //turn off autoreset

    sleep(1); //pause main thread long enough to ensure timer has timed out



    /** This third example demonstrates changing the callback function of the timer
     * and modifying the timer's timeout time from it's callback while autoreset is
     * enabled. In the TimerTimeout2 callback for the timer, we change the 
     * timer timeout time based on the timers current timeout time. We could change
     * the timers callback function from it's callback as well if desired.  */
    printf("\nDemonstrate timer with self reset in callback.\n");
    timer->AutoReset(true);  //turn on autoreset
    timer->setCallback(&TestClass::TimerTimeout2); //set a new callback function for the timer to call on timeout
    timer->setTime(1000); //set timer to 1 second timeout
    timer->Start();

    sleep(10);  //run timer for 10 seconds for demonstration

    timer->Stop(); //stop timer

    //end demonstration
  }

private:
  Timer<TestClass> *timer;
};



int main()
{
  TestClass test;
  test.start();

  return 0;
}
